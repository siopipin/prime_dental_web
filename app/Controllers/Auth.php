<?php
namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\Controller;

class Auth extends Controller
{
    public function index()
    {
        $data = [
            'title_meta' => view('partials/title-meta', ['title' => 'Login']),
            'title' => 'Login | Prime Dental Studio',
        ];
        return view('auth/login', $data);
    }

    public function register()
    {
        helper(['form']);

        $data = [
            'title_meta' => view('partials/title-meta', [
                'title' => 'Register',
            ]),
            'title' => 'Register | Prime Dental Studio',
        ];
        return view('auth/register', $data);
    }

    public function doRegister()
    {
        $data = [];
        helper(['form']);
        if ($this->request->getMethod() == 'post') {
            $validation = \Config\Services::validation();

            $rules = [
                'username' => [
                    'label' => 'Username',
                    'rules' => 'required|min_length[3]|max_length[20]',
                ],
                'email' => [
                    'label' => 'Email',
                    'rules' =>
                        'required|min_length[3]|max_length[20]|valid_email|is_unique[users.email]',
                ],
                'password' => [
                    'label' => 'Password',
                    'rules' => 'required|min_length[8]|max_length[20]',
                ],
                'password_confirm' => [
                    'label' => 'Confirm Password',
                    'rules' => 'matches[password]',
                ],
            ];

            if ($this->validate($rules)) {
                $user = new UserModel();
                $userdata = [
                    'username' => $this->request->getVar('username'),
                    'email' => $this->request->getVar('email'),
                    'password_confirm' => $this->request->getVar(
                        'password_confirm'
                    ),
                ];
                $user->add_user($userdata);
                $session = session();
                $session->setFlashData('success', 'Successful Registration');
                return redirect()->to('auth/login');
            } else {
                $data['validation'] = $validation->getErrors();
            }
        }

        echo view('auth/register', $data);
    }
}