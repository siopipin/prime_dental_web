<?php
namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users';

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $db = \Config\Database::connect();
        $builder = $db->table('users');
    }

    public function add_user($data)
    {
        $query = $this->db->table($this->table)->insert($data);
        $rows = $this->db->affected_rows();
        if ($rows > 0) {
            return $rows;
        } else {
            return false;
        }
    }
}